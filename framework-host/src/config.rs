use crate::modules::{ModuleError, ModuleRegistry};
use quick_error::quick_error;
use serde::{Deserialize, Serialize};
use serde_yaml::{Mapping, Value};
use std::collections::HashMap;
use std::fs::OpenOptions;
use std::path::Path;

quick_error! {
    #[derive(Debug)]
    pub enum ConfigError {
        Serde(err: serde_yaml::Error) {
            display("{}", err)
            from()
        }
        Io(err: std::io::Error) {
            display("{}", err)
            from()
        }
        InvalidType(msg: String) {
            display("{}", msg)
        }
    }
}

pub trait FromConfig {
    fn from_config(config: &ModuleConfig) -> Result<Self, ModuleError>
    where
        Self: Sized;
}

///
/// Contains the fields for a module
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ConfigModule {
    pub path: Option<String>,
    #[serde(default)]
    pub config: HashMap<String, Value>,
}

impl ConfigModule {
    pub fn merge(&mut self, other: Self) {
        for (key, value) in other.config {
            if !self.config.contains_key(&key) {
                self.config.insert(key, value);
            }
        }

        if let None = self.path {
            self.path = other.path;
        }
    }
}

///
/// Contains config structure for the application
///
#[derive(Clone, Debug, Default, Deserialize, Serialize)]
pub struct ModuleConfig {
    #[serde(default)]
    pub imports: Vec<String>,
    #[serde(default)]
    pub modules: HashMap<String, ConfigModule>,
    #[serde(default)]
    pub handlers: Mapping,
}

impl ModuleConfig {
    pub fn try_load<P: AsRef<Path>>(path: P) -> Result<Self, ConfigError> {
        Ok(serde_yaml::from_reader(
            OpenOptions::new().read(true).open(path)?,
        )?)
    }

    pub fn merge(mut self, other: Self) -> Self {
        for (name, module) in other.modules {
            if let Some(my_module) = self.modules.get_mut(&name) {
                my_module.merge(module);

                continue;
            }

            self.modules.insert(name, module);
        }

        for (name, listeners) in other.handlers {
            if let Some(Value::Sequence(my_listeners)) = self.handlers.get_mut(&name) {
                if let Value::Sequence(other_listeners) = listeners {
                    my_listeners.append(&mut other_listeners.to_vec());
                }
            } else {
                if let Value::Sequence(_) = &listeners {
                    self.handlers.insert(name, listeners);
                }
            }
        }

        self
    }
}

impl FromConfig for ModuleRegistry {
    fn from_config(config: &ModuleConfig) -> Result<Self, ModuleError> {
        let mut registry = Self::new();

        for (name, config) in &config.modules {
            registry.register(
                name,
                &config
                    .path
                    .as_ref()
                    .ok_or(ModuleError::UnableToLoadLibrary(format!(
                        "Module {} has no path set in any config (or was set to empty)",
                        name
                    )))?,
            )?;
        }

        Ok(registry)
    }
}
