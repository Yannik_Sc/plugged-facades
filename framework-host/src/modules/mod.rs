use framework_library::prelude::{
    CallableFunction, Container, Context, FromContext, IntoInjectable,
};
use framework_library::{
    Buffer, BufferError, CallError, ExternCallback, FunctionRequest,
};
use libloading::Library;
use quick_error::quick_error;
use serde::de::DeserializeOwned;
use serde::Serialize;
use serde_yaml::Value;
use std::collections::HashMap;
use std::ffi::OsStr;

use crate::config::ConfigError;


quick_error! {
    #[derive(Debug)]
    pub enum ModuleError {
        UnableToLoadLibrary(debug: String) {
            display("Unable to load library")
        }
        UnableToFindSymbol(name: String) {
            display("Unable to find symbol {}", name)
        }
        UnableToLoadModule(name: String) {
            display("Unable to find module {}", name)
        }
        ModuleExists(name: String) {
            display("Module is already registered {}", name)
        }
        Config(err: ConfigError) {
            from()
            display("{:?}", err)
        }
        Buffer(err: BufferError) {
            from()
            display("{:?}", err)
        }
    }
}

pub trait ModuleChannel {
    fn call_fn<F>(&self, _fun: F, args: impl Iterator<Item = Buffer>) -> Result<Buffer, CallError>
    where
        Self: Sized,
    {
        let mut request = FunctionRequest::new(std::any::type_name::<F>());

        for arg in args {
            request = request.add_raw_arg(arg);
        }

        self.call(&request)
    }

    fn call(&self, request: &FunctionRequest) -> Result<Buffer, CallError>;
}

/// A module that exists as a .so library on disk
pub struct SharedModule {
    library: Library,
}

/// A module that is builtin to the framework host application
pub struct BuiltinModule {
    methods: HashMap<String, Box<dyn CallableFunction>>,
    container: Container,
    extern_callback: ExternCallback,
}

impl BuiltinModule {
    pub fn new(extern_callback: ExternCallback) -> Self {
        Self {
            methods: Default::default(),
            container: Default::default(),
            extern_callback,
        }
    }

    pub fn register_service<S: 'static>(&mut self, service: S) {
        self.container.register(service);
    }

    pub fn register_method<
        A: FromContext + 'static,
        R: Serialize + 'static,
        I: IntoInjectable<A, R>,
    >(
        &mut self,
        method: I,
    ) {
        self.register_method_manual(std::any::type_name::<I>(), method);
    }

    pub fn register_method_manual<
        A: FromContext + 'static,
        R: Serialize + 'static,
        I: IntoInjectable<A, R>,
    >(
        &mut self,
        name: impl ToString,
        method: I,
    ) {
        self.methods
            .insert(name.to_string(), Box::new(method.into_injectable()));
    }
}

impl ModuleChannel for BuiltinModule {
    fn call(&self, request: &FunctionRequest) -> Result<Buffer, CallError> {
        if let Some(method) = self.methods.get(&request.name) {
            let mut context =
                Context::new(&self.container, self.extern_callback, request.args.clone());

            return Ok(method.call(&mut context));
        }

        Err(CallError::FunctionNotFound(request.name.clone()))
    }
}

impl SharedModule {
    pub fn new<P: AsRef<OsStr>>(path: P) -> Result<Self, ModuleError> {
        let library = unsafe {
            Library::new(&path)
                .map_err(|err| ModuleError::UnableToLoadLibrary(format!("{}", err)))?
        };

        Ok(Self { library })
    }
}

impl ModuleChannel for SharedModule {
    fn call(&self, request: &FunctionRequest) -> Result<Buffer, CallError> {
        let function = unsafe {
            self.library
                .get::<fn(&Buffer) -> Buffer>(request.name.as_bytes())
        };

        match function {
            Err(_) => Err(CallError::FunctionNotFound(request.name.clone())),
            Ok(function) => {
                let result = function(&Buffer::from_struct(&request.args))
                    .into_struct::<Result<Buffer, BufferError>>();

                result
                    .map_err(CallError::Buffer)?
                    .map_err(CallError::Buffer)
            }
        }
    }
}

pub struct ModuleRegistry {
    module_channels: HashMap<String, Box<dyn ModuleChannel>>,
}

impl ModuleRegistry {
    pub fn new() -> Self {
        Self {
            module_channels: Default::default(),
        }
    }

    pub fn insert_channel(
        &mut self,
        module_name: impl ToString,
        module: impl ModuleChannel + 'static,
    ) -> Result<(), ModuleError> {
        let module_name = module_name.to_string();

        {
            if self.module_channels.contains_key(&module_name) {
                return Err(ModuleError::ModuleExists(module_name));
            }
        }

        self.module_channels.insert(module_name, Box::new(module));

        Ok(())
    }

    pub fn register<N: ToString, P: AsRef<OsStr>>(
        &mut self,
        name: N,
        path: P,
    ) -> Result<(), ModuleError> {
        self.module_channels
            .insert(name.to_string(), Box::new(SharedModule::new(path)?));

        Ok(())
    }

    pub fn call_raw(&self, request: &FunctionRequest) -> Result<Buffer, ModuleError> {
        let request_name = request.name.clone();
        let mut request_name_parts = request_name.split("::");
        let module = request_name_parts.next();

        if let Some(module) = module {
            if let Some(channel) = self.module_channels.get(module) {
                return channel.call(request).map_err(|error| match error {
                    CallError::FunctionNotFound(name) => ModuleError::UnableToFindSymbol(name),
                    CallError::Buffer(error) => ModuleError::Buffer(error),
                });
            }
        }

        Err(ModuleError::UnableToLoadModule(
            module.unwrap_or("undefinded").to_string(),
        ))
    }

    pub fn call_fn<R: DeserializeOwned, F>(
        &self,
        _fun: F,
        args: impl Iterator<Item = Buffer>,
    ) -> Result<R, ModuleError> {
        let mut request = FunctionRequest::new(std::any::type_name::<F>());

        for arg in args {
            request = request.add_raw_arg(arg);
        }

        self.call_raw(&request)
            .map(|buffer| buffer.into_struct::<R>())?
            .map_err(ModuleError::Buffer)
    }

    pub fn setup_handles(&self, handles: &serde_yaml::Mapping) -> Result<(), ModuleError> {
        for (handler, handles) in handles.iter() {
            let handler = handler
                .as_str()
                .ok_or(ConfigError::InvalidType(
                    "Key of handlers has to be a string".to_string(),
                ))?
                .to_string();

            let handles = match handles {
                Value::Sequence(handles) => handles,
                _ => {
                    return Err(ModuleError::Config(ConfigError::InvalidType(
                        "Items in handles need to be arrays".to_string(),
                    )))
                }
            };

            for callbacks in handles.iter() {
                let callbacks = match callbacks {
                    Value::Mapping(callbacks) => callbacks,
                    _ => return Err(ModuleError::Config(ConfigError::InvalidType(
                        "The callbacks need to be an array with optional configuration/arguments"
                            .to_string(),
                    ))),
                };

                for (callback, arguments) in callbacks {
                    let callback = callback
                        .as_str()
                        .ok_or(ConfigError::InvalidType(
                            "Handler callback has to be a string".to_string(),
                        ))?
                        .to_string();

                    let mut request = FunctionRequest::new(&handler).add_arg(&callback);

                    match arguments {
                        Value::Sequence(seq) => {
                            for item in seq {
                                request = request.add_arg(item);
                            }
                        }
                        other => {
                            request = request.add_arg(other);
                        }
                    }

                    self.call_raw(&request)?;
                }
            }
        }

        Ok(())
    }
}
