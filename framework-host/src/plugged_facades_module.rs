use crate::config::ModuleConfig;
use crate::modules::{BuiltinModule, ModuleRegistry};
use framework_library::ExternCallback;
use std::collections::VecDeque;
use std::sync::{Arc, Mutex};
use std::thread::JoinHandle;

pub struct GetModuleRegistry(fn() -> &'static ModuleRegistry);

#[derive(Default, Clone)]
pub struct ThreadHandleContainer(Arc<Mutex<VecDeque<JoinHandle<()>>>>);

impl ThreadHandleContainer {
    pub fn pop_handle(&self) -> Option<JoinHandle<()>> {
        self.0.lock().unwrap().pop_front()
    }

    pub fn push_handle(&self, handle: JoinHandle<()>) {
        self.0.lock().unwrap().push_back(handle);
    }
}

impl GetModuleRegistry {
    pub fn get(&self) -> &'static ModuleRegistry {
        self.0()
    }
}

pub mod plugged_facades {
    use crate::config::ModuleConfig;
    use crate::plugged_facades_module::ThreadHandleContainer;
    use framework_library::prelude::{CallExtern, Data, Service};
    use framework_library::FunctionRequest;

    pub fn thread(
        function: Data<String>,
        call_extern: CallExtern,
        handle_container: Service<ThreadHandleContainer>,
    ) {
        let handle = std::thread::spawn(move || {
            println!("Spawning thread for {}", *function);
            call_extern.call_untyped(FunctionRequest::new(&*function));

            println!("Thread for {} stopped", *function);
        });

        handle_container.push_handle(handle);
    }

    pub fn send_config(
        function: Data<String>,
        config: Service<ModuleConfig>,
        call_extern: CallExtern,
    ) {
        let function_name = function.into_inner();
        let mut function_name_parts = function_name.split("::");
        let module = match function_name_parts.next() {
            None => return,
            Some(name) => name,
        };

        if let Some(module) = config.modules.get(module) {
            call_extern.call_untyped(FunctionRequest::new(function_name).add_arg(&module.config));
        }
    }

    pub fn send_extern_callback(function: Data<String>, call_extern: CallExtern) {
        let extern_callback = call_extern.clone().into_raw_callback();
        let ptr = extern_callback as usize;

        call_extern
            .call_untyped(FunctionRequest::new(function.into_inner()).add_arg(&(ptr as usize)));
    }
}

pub fn build_plugged_facades(
    get_module_registry: fn() -> &'static ModuleRegistry,
    module_callback: ExternCallback,
    config: ModuleConfig,
    thread_handle_container: ThreadHandleContainer,
) -> BuiltinModule {
    let mut module = BuiltinModule::new(module_callback);
    module.register_service(GetModuleRegistry(get_module_registry));
    module.register_service(config);
    module.register_service(thread_handle_container);
    module.register_method_manual("plugged_facades::thread", plugged_facades::thread);
    module.register_method_manual("plugged_facades::send_config", plugged_facades::send_config);
    module.register_method_manual(
        "plugged_facades::send_extern_callback",
        plugged_facades::send_extern_callback,
    );

    module
}
