use std::path::Path;
use framework_host::config::{ConfigError, FromConfig, ModuleConfig};
use framework_host::modules::{ModuleError, ModuleRegistry};
use framework_host::plugged_facades_module::{build_plugged_facades, ThreadHandleContainer};
use framework_library::prelude::Buffer;
use framework_library::FunctionRequest;

static mut MODULE_REGISTRY: Option<ModuleRegistry> = None;

pub fn get_module_registry() -> &'static ModuleRegistry {
    unsafe {
        MODULE_REGISTRY
            .as_ref()
            .expect("Module Registry not initialized yet!")
    }
}

extern "C" fn module_callback(buf: Buffer) -> Buffer {
    let request: FunctionRequest = buf.into_struct().unwrap();

    match get_module_registry().call_raw(&request) {
        Ok(buf) => return buf,
        Err(_) => {
            eprintln!(
                "Could not find function {} in modules.",
                &request.name
            );
        }
    }

    Buffer::from_struct(&())
}

pub fn load_config_recurse<P: AsRef<Path>>(config: ModuleConfig, path: P) -> Result<ModuleConfig, ConfigError>{
    let mut other_config = ModuleConfig::try_load(path)?;
    let imports = other_config.imports.clone();

    for import in &imports {
        other_config = load_config_recurse(other_config, import)?;
    }

    Ok(config.merge(other_config))
}

pub fn get_config() -> Result<ModuleConfig, ConfigError> {
    let mut args = std::env::args().skip(1);
    let config_path = args.next();

    if config_path.is_none() {
        eprintln!("Config path is missing!");

        std::process::exit(1);
    }

    load_config_recurse(ModuleConfig::default(), config_path.unwrap())
}

pub fn modules_from_config(config: &ModuleConfig) -> Result<ModuleRegistry, ModuleError> {
    let registry = ModuleRegistry::from_config(config)?;

    Ok(registry)
}

fn main() -> Result<(), Box<dyn std::error::Error>>{
    let config = get_config().expect("Could not load config");

    let handlers = config.handlers.clone();
    let mut reg = modules_from_config(&config)?;
    let thread_handles = ThreadHandleContainer::default();
    reg.insert_channel(
        "plugged_facades",
        build_plugged_facades(
            get_module_registry,
            module_callback,
            config,
            thread_handles.clone(),
        ),
    )
    .ok();

    unsafe {
        MODULE_REGISTRY = Some(reg);
    }

    let reg = get_module_registry();

    reg.setup_handles(&handlers)
        .expect("Could not setup handlers");

    while let Some(handle) = thread_handles.pop_handle() {
        if let Err(err) = handle.join() {
            println!("Thread died: {:?}", err);
        }
    }

    Ok(())
}
