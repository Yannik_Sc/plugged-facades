use framework_library::prelude::*;
use framework_library::{FacadeFunctionHolder, FunctionRequest};
use serde::{Deserialize, Serialize};

struct GreetingService(String);

impl GreetingService {
    pub fn greet(&self) {
        println!("Hello {}", self.0);
    }
}

pub struct Other;

pub struct OtherTest;

#[derive(Serialize, Deserialize, Debug)]
pub struct DataStruct {
    name: String,
}

fn test(
    d1: Data<DataStruct>,
    d2: Option<Data<DataStruct>>,
    test_service: Service<GreetingService>,
    _other: Service<Other>,
    other: Result<Service<OtherTest>, ContainerError>,
) {
    println!("{:?}", d1.into_inner());
    println!("{:?}", d2.and_then(|data| Some(data.into_inner())));
    println!("{:?}", other.err());

    test_service.greet();
}

extern "C" fn external(buf: Buffer) -> Buffer {
    buf
}

pub fn main() {
    let container = Container::default();
    container.register(GreetingService(String::from("world")));
    container.register(Other);

    let mut facade = FacadeFunctionHolder::new("injection_test", external, container);
    facade.register(test);

    println!("{:#?}", facade.get_description());

    facade
        .call_untyped(FunctionRequest::new("test").add_arg(&DataStruct {
            name: String::from("Test Name"),
        }))
        .ok();
}
