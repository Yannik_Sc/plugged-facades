use proc_macro::TokenStream;
use std::ops::Index;
use std::str::FromStr;
use syn::__private::ToTokens;
use syn::parse::{Parse, ParseStream};
use syn::punctuated::Punctuated;
use syn::{parenthesized, ReturnType, Token, Type, UseTree};

#[derive(Debug)]
enum MacroError {
    UnableToTokenize(String),
    FunctionMultiImportUnsupported,
}

type MacroResult<T> = Result<T, MacroError>;

struct ExportFunctionStruct {
    pub parent_fn: UseTree,
    pub inputs: Punctuated<Type, Token![,]>,
    pub output: ReturnType,
}

impl Parse for ExportFunctionStruct {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        input.parse::<Option<Token![::]>>()?;
        let parent_fn = input.parse()?;
        let content;
        parenthesized!(content in input);
        let inputs = content.parse_terminated(Type::parse)?;
        let output = input.parse()?;

        Ok(Self {
            parent_fn,
            inputs,
            output,
        })
    }
}

impl ExportFunctionStruct {
    pub fn build_api_function(&self) -> MacroResult<proc_macro::TokenStream> {
        let mut signature = self.build_api_function_signature()?;
        let body = self.build_api_function_body()?;
        signature.extend(vec![proc_macro::TokenStream::from(body)]);

        Ok(signature)
    }

    pub fn build_api_function_signature(&self) -> MacroResult<proc_macro::TokenStream> {
        let mut args = Vec::new();
        let ret_typ = self.get_output_arg();

        for index in 0..self.inputs.len() {
            let typ = self.inputs.index(index);
            args.push(format!("arg_{}: {}", index, typ.to_token_stream()));
        }

        TokenStream::from_str(format!(
            r#"pub fn {fun_name}({args}) -> framework_library::prelude::FunctionRequestWithReturn<{ret_typ}>"#,
            fun_name = Self::get_function_name(&self.parent_fn)?,
            args = args.join(","),
            ret_typ = ret_typ,
        ).as_str()).map_err(|_| MacroError::UnableToTokenize("Could not build api function signature".to_string()))
    }

    pub fn build_api_function_body(&self) -> MacroResult<proc_macro::TokenStream> {
        let mut add_args = Vec::new();
        let ret_typ = self.get_output_arg();
        let mut orig_fun_path = Vec::new();
        Self::get_original_function_path(&self.parent_fn, &mut orig_fun_path)?;
        for index in 0..self.inputs.len() {
            add_args.push(format!(".add_arg(&arg_{})", index));
        }

        TokenStream::from_str(
            format!(
                r#"
            {{
                framework_library::prelude::FunctionRequest::function({fun_name})
                    {add_args}
                    .with_return::<{ret_typ}>()
            }}
        "#,
                fun_name = orig_fun_path.join("::"),
                ret_typ = ret_typ,
                add_args = add_args.join(""),
            )
            .as_str(),
        )
        .map_err(|_| MacroError::UnableToTokenize("Could not build api function body".to_string()))
    }

    fn get_output_arg(&self) -> String {
        let ret_typ = &self.output;

        if ret_typ.to_token_stream().is_empty() {
            return String::from("()");
        }

        format!(
            "{}",
            match ret_typ {
                ReturnType::Default => String::from("()"),
                ReturnType::Type(_, typ) => format!("{}", typ.to_token_stream().to_string()),
            }
        )
    }

    fn get_function_name(tree: &UseTree) -> MacroResult<String> {
        Ok(match tree {
            UseTree::Path(path) => Self::get_function_name(path.tree.as_ref())?,
            UseTree::Name(name) => name.to_token_stream().to_string(),
            UseTree::Rename(rename) => rename.rename.to_string(),
            UseTree::Glob(_) | UseTree::Group(_) => {
                return Err(MacroError::FunctionMultiImportUnsupported);
            }
        })
    }

    fn get_original_function_path(tree: &UseTree, parts: &mut Vec<String>) -> MacroResult<()> {
        match tree {
            UseTree::Path(path) => {
                parts.push(path.ident.to_string());

                Self::get_original_function_path(path.tree.as_ref(), parts)?;
            }
            UseTree::Name(name) => {
                parts.push(name.to_token_stream().to_string());
            }
            UseTree::Rename(rename) => parts.push(rename.ident.to_string()),
            UseTree::Glob(_) | UseTree::Group(_) => {
                return Err(MacroError::FunctionMultiImportUnsupported);
            }
        }

        Ok(())
    }
}

#[proc_macro]
pub fn api_fn(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let export_fn = syn::parse_macro_input!(input as ExportFunctionStruct);

    export_fn.build_api_function().unwrap()
}
