# Benchmark Encoding/Decoding

This benchmark compares the encoding and decoding of a struct using different libraries.

## Test code

With the aspect of FFI in mind the minimal used struct for the conversion comes with a `#[repr(C)]`.

```rust
use bincode::{Encode, Decode};
use serde::{Deserialize, Serialize};

#[repr(C)]
#[derive(Clone, Debug, Deserialize, Serialize, Encode, Decode)]
struct Person {
    name: String,
    age: u8,
}
```

To test the combined speed of serialization and deserialization the respective methods are called 1.000.000 times in a
loop.

```rust
fn encode<T>(value: T) -> Vec<u8> { todo!() }

fn decode<T>(bytes: &[u8]) -> T { todo!() }

fn main() {
    let mut bob = Person {
        name: "bob".to_string(),
        age: 22,
    };

    for _ in 1..1_000_000 {
        let enc = encode(&bob)?;

        bob = decode(enc.as_slice())?;
    }
}
```

**NOTE:** The `encode` and `decode` functions are different for the respective libraries.

## Results

The here displayed time values were read using the `time` command on the built
executable (`time target/debug/framework-host`).

| Library    | Duration for 1M runs                                                       | Note                                              |
|------------|----------------------------------------------------------------------------|---------------------------------------------------|
| bincode    | target/debug/framework-host  4.28s user 0.00s system 99% cpu 4.294 total   | This were run with the Encode/Decode from Bincode |
| bincode    | target/debug/framework-host  4.84s user 0.00s system 99% cpu 4.861 total   | This was done using the `bincode::serde` methods  |
| rmp-serde  | target/debug/framework-host  5.77s user 0.00s system 99% cpu 5.797 total   |                                                   |
| serde_json | target/debug/framework-host  12.46s user 0.00s system 99% cpu 12.518 total |                                                   |
| ron        | target/debug/framework-host  34.07s user 0.01s system 99% cpu 34.279 total |                                                   |

**NOTE:** The test used debug builds, the results might differ when using release builds

### Release results

These results were done with `cargo build --release` and `time target/release/framework-host`.

| Library    | Duration for 1M runs                                                       | Note                                              |
|------------|----------------------------------------------------------------------------|---------------------------------------------------|
| bincode    | target/release/framework-host  0.20s user 0.00s system 99% cpu 0.202 total | This were run with the Encode/Decode from Bincode |
| bincode    | target/release/framework-host  0.27s user 0.00s system 99% cpu 0.268 total | This was done using the `bincode::serde` methods  |
| rmp-serde  | target/release/framework-host  0.20s user 0.00s system 99% cpu 0.198 total |                                                   |

## Conclusion

Although the first result table shows, that bincode native is clearly the winner, after repeating the tests with release
builds the values clearly show, that rmp-serde is slightly faster. Not only due to the timing, but especially due to the
fact, that serde itself is a big ecosystem with various libraries for all sorts of formats, the decision falls on
rmp-serde. It is faster or at least similarly fast to bincode native, and allows the struct to only derive the serde
traits which is cleaner and opens way more options for handling the structs in practice

# Benchmark Wasmtime vs Libloading

This benchmark part shows weather this framework should use wasm for portable, modular code, or libloading for better
performance (it this is the case).

## Test code

Due to the big difference in libloading and wasmtime, the code for both cases look quite different

### Wasmtime

Library code for wasmtime

```rust
#[no_mangle]
pub extern "C" fn __wasm_get_module(len: &mut u32, ptr: &mut i32) {
    let v = framework_library::to_vec(&get_module()).unwrap_or_default();

    *len = v.len() as u32;
    *ptr = v.as_slice().as_ptr() as i32;
}

fn get_module() -> framework_library::ModuleDefinition {
    framework_library::ModuleDefinition {
        exported_functions: vec![
            framework_library::ExportedFunction { function_name: "test_function".to_string() }
        ]
    }
}
```

Loading code

```rust
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let engine = wasmtime::Engine::default();
    let module = wasmtime::Module::new(&engine, std::fs::read("target/wasm32-unknown-unknown/release/example_module.wasm")?)?;
    let mut store = wasmtime::Store::new(&engine, ());
    let instance = wasmtime::Instance::new(&mut store, &module, &[])?;
    let memory = instance.get_memory(&mut store, "memory").unwrap();
    let mut data = framework_library::ModuleDefinition {
        exported_functions: vec![],
    };

    for _ in 0..1_000_000 {
        let fun = instance.get_func(&mut store, "__wasm_get_module").unwrap();// exports.get_function("__wasm_get_module")?;
        let typed = fun.typed::<(i32, i32), (), _>(&store)?;
        let val = memory.data_size(&store);

        let mut buf: &mut [u8] = &mut [0, 0, 0, 0];
        let mut buf2: &mut [u8] = &mut [0, 0, 0, 0];

        memory.write(&mut store, (val - 4) as usize, buf)?;
        memory.write(&mut store, (val - 8) as usize, buf2)?;

        typed.call(&mut store, ((val - 4) as i32, (val - 8) as i32))?;

        memory.read(&store, (val - 4) as usize, &mut buf)?;
        memory.read(&store, (val - 8) as usize, &mut buf2)?;

        let len = u32::from_le_bytes(buf.try_into()?);
        let ptr = i32::from_le_bytes(buf2.try_into()?);

        let mut buf = vec![0_u8; len as usize];
        memory.read(&store, ptr as usize, buf.as_mut_slice());

        data = framework_library::from_slice(buf.as_slice())?;
    }

    println!("{:#?}", data);
    Ok(())
}
```

### Libloading

Library code

```rust
#[no_mangle]
pub extern "C" fn __wasm_get_module() -> Vec<u8> {
    let v = framework_library::to_vec(&get_module()).unwrap_or_default();

    v
}

fn get_module() -> framework_library::ModuleDefinition { todo!("Same as wasmtime version") }
```

Loading code

```rust
pub fn main() -> Result<(), Box<dyn std::error::Error>> {
    unsafe {
        let library = libloading::Library::new("target/release/libexample_module.so")?;
        let mut definition = framework_library::ModuleDefinition {
            exported_functions: vec![]
        };

        for _ in 0..1_000_000 {
            let func: libloading::Symbol<extern "C" fn() -> Vec<u8>> = library.get(b"__wasm_get_module")?;
            let value: Vec<u8> = func();
            definition = framework_library::from_slice(value.as_slice())?;
        }

        println!("{:#?}", definition);
    }

    Ok(())
}
```

## Results

All the above code is compiled with `--release`. For the wasm example the target `--target wasm32-unknown-unknown` is 
used.

| Library    | Time                                                                                  |
|------------|---------------------------------------------------------------------------------------|
| libloading | target/release/examples/libloading_bench  0.82s user 0.00s system 99% cpu 0.824 total |
| wasmtime   | target/release/framework-host  1.64s user 0.03s system 106% cpu 1.568 total           |
| wasmer     | target/release/framework-host  1.18s user 0.01s system 105% cpu 1.120 total           |

For comparison, I later decided to take wasmer into account, as it promotes better performance as wasmtime, which it, in
fact, provides. Though performance vs complexity in the wasm case in general shows to me, that it is not ready for 
production yet.

## Conclusion

Due to the big complexity with wasm and the fact, that libloading achieves the same in roughly half the time, the 
decision here falls on libloading and dynamic libraries for modules.
