# Plugged Facades

This projects aims to create a modular framework which allows modules to be loaded and executed to form a
complete application.

## Principles

### Facades

Facades are the exposed functions of a module. Input and output of a module is done through transfer structs, which have
to implement `serde::{Deserialize, Serialize}` traits.

#### Transfering of the data structs

To transfer the data structs between the main application and the modules, the data structs will be
encoded and decoded using rmp-serde. Which is the actual reason why the structs have to implement the Serialize and
Deserialize traits.

For an overview, why rmp-serde is used take a look at the [benchmark.md](benchmark.md).

### Events (TODO)

Events are used to allow modules to start working when something, somewhere else, happens.


## Try it out!

### Without explanation

```bash
# Clone and setup repo
git clone https://gitlab.com/Yannik_Sc/plugged-facades.git

cd plugged-facades

mkfifo commands.fifo

# Run project with currently, testing modules
cargo run -- config.yaml
```

In a new shell

```bash
# Send the greet command to the application. Shell 1 will print "Hello world"
cargo run --bin run-command --features=run-command -- greet world

# Stops the application
cargo run --bin run-command --features=run-command -- stop
```

### Some explanation whats happening

You can simply run the project with cargo and the provided config.yaml `cargo run -- config.yaml`. For the project to 
work however, you need a fifo which is called "commands.fifo". You can simply create it using the `mkfifo commands.fifo`
command.

When started the application will listen for commands sent through the `commands.fifo`. It expects a simple json structure
containing a `name: String` and `args: Vec<Json>` field.

For testing there is currently the greet command, which expects a single string argument, which is a name that is greeted.
The command is provided by the example module. You can use the run-command helper provided by the command-api-module to
send commands to the application. It can be called using 
`cargo run --bin run-command --features=run-command -- [COMMAND] [ARGS...]`. **Note**: there is no feedback if the called
command does not exist.

For stopping the application you can either press Ctrl + C, or send the stop command. (The signal send by Ctrl + C is not
yet propagated to the modules, so the module threads will be killed.)

