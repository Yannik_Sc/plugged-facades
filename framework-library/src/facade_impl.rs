use crate::container::{Context, FromContext, IntoInjectable};
use crate::{
    Buffer, CallError, Container, ExternCallback, FacadeDescription, FacadeFunctionHolder,
    FunctionRequest, FunctionRequestWithReturn,
};
use serde::Serialize;

impl FunctionRequest {
    pub fn function<T>(_: T) -> Self {
        Self::new(std::any::type_name::<T>())
    }

    pub fn new(name: impl ToString) -> Self {
        Self {
            name: name.to_string(),
            args: vec![],
        }
    }

    pub fn add_arg<T: Serialize>(mut self, data: &T) -> Self {
        self.args.push(Buffer::from_struct(data));

        self
    }

    pub fn add_raw_arg(mut self, buffer: Buffer) -> Self {
        self.args.push(buffer);

        self
    }

    pub fn with_return<R>(self) -> FunctionRequestWithReturn<R> {
        FunctionRequestWithReturn {
            request: self,
            _return_type: Default::default(),
        }
    }
}

impl<R> FunctionRequestWithReturn<R> {
    pub fn name(&self) -> String {
        self.request.name.clone()
    }

    pub fn inner_ref(&self) -> &FunctionRequest {
        &self.request
    }

    pub fn into_inner(self) -> FunctionRequest {
        self.request
    }
}

impl FacadeFunctionHolder {
    pub fn new(
        name: impl ToString,
        external_callback: ExternCallback,
        container: Container,
    ) -> Self {
        Self {
            name: name.to_string(),
            container,
            functions: Default::default(),
            external_callback,
        }
    }

    pub fn register<
        A: FromContext + 'static,
        R: serde::Serialize + 'static,
        T: IntoInjectable<A, R> + 'static,
    >(
        &mut self,
        function: T,
    ) -> &mut Self {
        self.functions.insert(
            std::any::type_name::<T>().to_string(),
            Box::new(function.into_injectable()),
        );

        self
    }

    pub fn call<R: serde::de::DeserializeOwned>(
        &self,
        request: FunctionRequestWithReturn<R>,
    ) -> Result<R, CallError> {
        Ok(self
            .call_untyped(request.into_inner())?
            .into_struct()
            .map_err(CallError::Buffer)?)
    }

    pub fn call_untyped(&self, request: FunctionRequest) -> Result<Buffer, CallError> {
        let function = self
            .functions
            .get(&request.name)
            .ok_or(CallError::FunctionNotFound(request.name.clone()))?;
        let mut context = self.build_context(request.args);

        Ok(function.call(&mut context))
    }

    pub fn get_description(&self) -> FacadeDescription {
        FacadeDescription {
            name: self.name.clone(),
            functions: self
                .functions
                .keys()
                .map(String::clone)
                .collect::<Vec<String>>(),
        }
    }

    fn build_context(&self, args: Vec<Buffer>) -> Context {
        Context::new(&self.container, self.external_callback, args)
    }
}
