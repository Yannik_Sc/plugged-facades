#[macro_export]
macro_rules! exports {
    (let container = $container: expr ; let extern_callback = $extern_callback: expr ; mod $modname: ident { $(fn $name: ident;)* }; $(addon $addon: tt $( ( $($args: tt)* ) )? ; )*) => {
        $(
            framework_library::export_addons!{
                $modname $container; addon $addon $( $($args)* )?
            }
        )*

        mod $modname {
            $(
               framework_library::paste!{
                    #[export_name = $modname "::" $name]
                    pub extern "C" fn $name(buffer: &framework_library::Buffer) -> framework_library::Buffer {
                        use framework_library::prelude::*;
                        let args = buffer.to_struct::<Vec<framework_library::Buffer>>();

                        Buffer::from_struct(&args.map(|args| {
                            let mut context = Context::new($container, $extern_callback, args);

                            let injectable = crate::$name.into_injectable();
                            injectable.call(&mut context)
                        }))
                    }
                }
            )*
        }
    };
}

#[macro_export]
macro_rules! export_addons {
    ($modname: ident $container: expr; addon extern_callback) => {
        #[allow(non_snake_case)]
        mod extern_callback__intern {
            use framework_library::prelude::*;

            pub(crate) static mut EXTERN_CALLBACK: Option<ExternCallback> = None;

            fn extern_callback_intern(buffer: &Buffer) -> Result<Buffer, BufferError> {
                let args = buffer.to_struct::<Vec<Buffer>>()?;
                let arg = args.into_iter().nth(0).ok_or(BufferError::TypeError)?;
                let arg = arg.into_struct::<usize>()?;

                unsafe { EXTERN_CALLBACK = Some(std::mem::transmute::<_, ExternCallback>(arg)) };

                Ok(Buffer::from_struct(&()))
            }

            framework_library::paste! {
                #[export_name = $modname "::extern_callback"]
                pub extern "C" fn extern_callback(buffer: &Buffer) -> Buffer {
                    Buffer::from_struct(&extern_callback_intern(buffer))
                }
            }
        }

        extern "C" fn extern_callback(
            buffer: framework_library::Buffer,
        ) -> framework_library::Buffer {
            if let Some(callback) = unsafe { extern_callback__intern::EXTERN_CALLBACK } {
                return callback(buffer);
            }

            Buffer::from_struct(&())
        }
    };

    ($modname: ident $container: expr; addon config $config_type: ty) => {
        mod config__intern {
            use framework_library::prelude::*;
            fn set_config_intern(buffer: &Buffer) -> Result<Buffer, BufferError> {
                let args = buffer.to_struct::<Vec<Buffer>>()?;
                let arg = args.into_iter().nth(0).ok_or(BufferError::TypeError)?;
                let config = arg.into_struct::<$config_type>()?;

                $container.register(config);

                Ok(Buffer::from_struct(&()))
            }

            framework_library::paste! {
                #[export_name = $modname "::config"]
                pub extern "C" fn set_config(buffer: &Buffer) -> Buffer {
                    Buffer::from_struct(&set_config_intern(buffer))
                }
            }
        }
    };

    ($modname: ident $container: expr; addon container $callable: path) => {
        mod container__intern {
            use framework_library::prelude::*;

            pub(crate) static mut CONTAINER: Option<Container> = None;

            #[inline(always)]
            pub(crate) fn build_container() {
                unsafe {
                    if CONTAINER.is_none() {
                        CONTAINER = Some($callable(Container::default()));
                    }
                };
            }
        }

        pub fn get_container() -> &'static framework_library::prelude::Container {
            use std::ops::Deref;
            container__intern::build_container();

            unsafe { container__intern::CONTAINER.as_ref().unwrap() }
        }
    };
}
