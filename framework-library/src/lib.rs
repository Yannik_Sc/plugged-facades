//!
//! # Plugged Facades
//!
//! Plugged facades is a way to write modular, thus type-safe applications using modules and facades
//!
//! ## Modules
//!
//! Modules are represented as libraries.
//!
//! They can (theoretically) contain multiple facades, this however, is not really tested yet and
//! never used. Most parts are built to allow this though.
//!
//! ## Facades
//!
//! Facades expose the functions of a module.
//!
//! Functions are focused on data structs. Facades handle "requests" and give response to them.
//!
//! Data structs have to be serializable and deserializable using the corresponding serde traits.
//! All the data is wrapped using the `Buffer`, which transfers the data as bytes between modules
//! and the host application. Facades can import Facades from other modules to provide interaction
//! between modules. An example for this can be seen between the `example-module` and the
//! `command-api-module`.
//!
//! Facades can contain a `main` function, which allows them to start something. This is not
//! required as modules can also only contain library-sort of code. If this is used however, keep in
//! mind, that your facade will run in its own thread. Having globally-mutable const or static data
//! will result in strange behaviour if the data is not properly secured.
//!
//! ## Injectable Functions
//!
//! All Facade functions are injectable functions (when using the `export` macro). By this the
//! function can request access to services, facades and arguments.
//!
//! Services are provided using the `Container`. Services need to be registered beforehand though.
//!
//! For optional or *maybe-failing* data (or services), the `Data` and `Service` types can be
//! wrapped using rusts `Option` and `Result` types. If something is requested, which is not set,
//! and `Option` and `Result` is not used, the application will panic. Which may or may not result
//! in a total crash of the application (as facades are running in their own thread).
//!

mod buffer_impl;
mod container;
mod facade_impl;
mod macros;

pub use rmp_serde::{from_slice, to_vec};
pub use serde::{Deserialize, Serialize};

use quick_error::quick_error;
use std::collections::HashMap;
use std::fmt::Debug;
use std::marker::PhantomData;

use crate::container::{CallableFunction, Container};

pub mod prelude {
    pub use crate::container::*;
    pub use crate::macros::*;
    pub use crate::{
        Buffer, BufferError, ExternCallback, FacadeDescription, FacadeFunctionHolder,
        FunctionRequest, FunctionRequestWithReturn,
    };
    pub use framework_macro::api_fn;
}

pub use framework_macro::api_fn;
pub use paste::paste;

///
/// An FFI-Safe way of exchanging data structs
///
#[repr(C)]
pub struct Buffer {
    pub len: usize,
    pub boxed: *mut Vec<u8>,
    pub data: *const u8,
}

///
/// The callback that is used to send requests to the host application
///
pub type ExternCallback = extern "C" fn(Buffer) -> Buffer;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub enum ConvertDirection {
    From(String),
    To(String),
}

impl ConvertDirection {
    pub fn from_type<T>() -> Self {
        Self::From(std::any::type_name::<T>().to_string())
    }

    pub fn to_type<T>() -> Self {
        Self::To(std::any::type_name::<T>().to_string())
    }
}

quick_error! {
    #[derive(Debug, Deserialize, Serialize)]
    pub enum BufferError {
        TypeError {
            display("Could not convert to given type")
        }
        ConversionError(direction: ConvertDirection) {
            display("Buffer could not convert {:?} struct", direction)
        }
    }
}

quick_error! {
    #[derive(Debug)]
    pub enum CallError {
        FunctionNotFound(name: String) {
            display("Called function was not found")
        }
        Buffer(err: crate::BufferError) {
            from()
        }
    }
}

///
/// Holds all the info, that is needed for the facade to make a function call.
///
pub struct FacadeFunctionHolder {
    pub name: String,
    container: Container,
    functions: HashMap<String, Box<dyn CallableFunction>>,
    external_callback: ExternCallback,
}

///
/// Contains the minimal information to call a function
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FunctionRequest {
    pub name: String,
    pub args: Vec<Buffer>,
}

///
/// Same as `FunctionRequest`. Additionally Contains the return type.
///
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FunctionRequestWithReturn<R> {
    request: FunctionRequest,
    _return_type: PhantomData<R>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct FacadeDescription {
    pub name: String,
    pub functions: Vec<String>,
}
