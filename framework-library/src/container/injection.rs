use crate::container::{
    Container, Context, Data, FromContainer, FromContext, Injectable, IntoInjectable, Service,
};
use crate::prelude::{CallExtern, ContainerError, DataError};
use crate::{Buffer, BufferError, ExternCallback, FunctionRequest, FunctionRequestWithReturn};
use serde::de::DeserializeOwned;
use std::ops::Deref;
use std::sync::Arc;

impl<T> Deref for Service<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.inner.as_ref()
    }
}

impl<T> Deref for Data<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl<T> Data<T> {
    pub fn into_inner(self) -> T {
        self.data
    }
}

impl<T> Service<T> {
    pub fn into_inner(self) -> Arc<T> {
        self.inner
    }
}

impl<T: 'static> FromContainer for Service<T> {
    fn from_container(container: &Container) -> Self {
        Service {
            inner: container.get().unwrap_or_else(|err| panic!("{}", err)),
        }
    }
}

impl<T: 'static> FromContainer for Option<Service<T>> {
    fn from_container(container: &Container) -> Self {
        if let Ok(service) = container.get() {
            return Some(Service { inner: service });
        }

        None
    }
}

impl<T: 'static, E: From<ContainerError>> FromContainer for Result<Service<T>, E> {
    fn from_container(container: &Container) -> Self {
        Ok(Service {
            inner: container.get()?,
        })
    }
}

impl<T: DeserializeOwned> FromContext for Data<T> {
    fn from_context(context: &mut Context) -> Self {
        let slice = context.args.next().unwrap();

        Data {
            data: slice.into_struct().unwrap(),
        }
    }
}

impl<T: DeserializeOwned> FromContext for Option<Data<T>> {
    fn from_context(context: &mut Context) -> Self {
        if let Some(data) = context.args.next() {
            if let Some(data) = data.into_struct().ok() {
                return Some(Data { data });
            }
        }

        None
    }
}

impl<T: DeserializeOwned, E: From<DataError>> FromContext for Result<Data<T>, E> {
    fn from_context(context: &mut Context) -> Self {
        let data_raw = context.args.next().ok_or(DataError::NoneError)?;
        let data = data_raw.into_struct().map_err(|_| DataError::TypeError)?;

        Ok(Data { data })
    }
}

impl<A, R> Injectable<A, R> {
    pub fn new(function: impl Fn(A) -> R + 'static) -> Self {
        Self {
            function: Box::new(function),
        }
    }
}

impl<'a> Context<'a> {
    pub fn new(
        container: &'a Container,
        call_function: ExternCallback,
        args: impl IntoIterator<Item = Buffer> + 'static,
    ) -> Self {
        Self {
            container,
            call_function,
            args: Box::new(args.into_iter()),
        }
    }
}

impl CallExtern {
    pub fn new(callback: ExternCallback) -> Self {
        Self { callback }
    }

    pub fn call_raw(&self, buf: Buffer) -> Buffer {
        (self.callback)(buf)
    }

    pub fn call_untyped(&self, request: FunctionRequest) -> Buffer {
        self.call_raw(Buffer::from_struct(&request))
    }

    pub fn call<R: DeserializeOwned>(
        &self,
        request: FunctionRequestWithReturn<R>,
    ) -> Result<R, BufferError> {
        let result_buffer = self.call_raw(Buffer::try_from_struct(&request.request)?);

        Ok(result_buffer.into_struct::<R>()?)
    }

    pub fn into_raw_callback(self) -> ExternCallback {
        self.callback
    }
}

impl FromContext for CallExtern {
    fn from_context(context: &mut Context) -> Self {
        Self {
            callback: context.call_function,
        }
    }
}

impl<A: FromContext, R> Injectable<A, R> {
    pub fn run(&self, context: &mut Context) -> R {
        (self.function)(A::from_context(context))
    }
}

impl<R, F: Fn() -> R + 'static> IntoInjectable<(), R> for F {
    fn into_injectable(self) -> Injectable<(), R> {
        Injectable::new(move |_| self())
    }
}

macro_rules! injection_from_tuple {
    () => {};
    ($A: ident, $($arg: ident,)*) => {
        paste::paste!{
            impl<Fun: Fn($A, $($arg,)*) -> Ret + 'static, Ret, $A: FromContext, $($arg: FromContext,)*> IntoInjectable<($A, $($arg,)*), Ret> for Fun {
                fn into_injectable(self) -> Injectable<($A, $($arg,)*), Ret> {
                    Injectable::new(
                        move |([<arg_ $A:lower>], $([<arg_ $arg:lower>],)*)|
                        self([<arg_ $A:lower>], $([<arg_ $arg:lower>],)*)
                    )
                }
            }
        }

        injection_from_tuple!($($arg,)*);
    };
}

injection_from_tuple!(
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,
);
