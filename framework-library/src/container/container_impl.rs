use crate::container::Container;
use crate::prelude::ContainerError;
use std::sync::Arc;

unsafe impl Send for Container {}

unsafe impl Sync for Container {}

impl Container {
    pub fn get<T: std::any::Any + 'static>(&self) -> Result<Arc<T>, ContainerError> {
        let name = std::any::type_name::<T>();
        let services = self.services.read().unwrap();
        let service = services
            .get(name)
            .ok_or(ContainerError::NoneError(name.to_string()))?;
        let service = service
            .downcast_ref::<Arc<T>>()
            .ok_or(ContainerError::TypeError)?;

        Ok(Arc::clone(service))
    }

    pub fn register<T: 'static>(&self, service: T) {
        self.services.write().unwrap().insert(
            std::any::type_name::<T>().to_string(),
            Box::new(Arc::new(service)),
        );
    }
}
