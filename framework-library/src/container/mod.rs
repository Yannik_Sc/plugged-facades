mod container_impl;
mod from_container;
mod injection;

use crate::{Buffer, ExternCallback};
use quick_error::quick_error;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::sync::{Arc, RwLock};

quick_error! {
    #[derive(Debug)]
    pub enum ContainerError {
        NoneError(name: String) {
            display("Service '{}' does not exist", name)
        }
        TypeError {
            display("Service does not match type")
        }
    }
}

quick_error! {
    #[derive(Debug, Deserialize, Serialize)]
    pub enum DataError {
        NoneError {
            display("Data does not exist")
        }
        TypeError {
            display("Data type is not matching")
        }
    }
}

///
/// Houses all needed services.
/// Identifies services by its `std::any::type_name`
///
#[derive(Default)]
pub struct Container {
    services: RwLock<HashMap<String, Box<dyn std::any::Any>>>,
}

///
/// Contains the app container, call args and the extern callback function.
/// Used to call `Injectable` Functions.
///
pub struct Context<'a> {
    pub container: &'a Container,
    pub call_function: ExternCallback,
    pub args: Box<dyn Iterator<Item = Buffer>>,
}

///
/// Gets data (arguments) from the context
///
pub struct Data<T> {
    data: T,
}

///
/// Gets a Service from the container
///
pub struct Service<T> {
    inner: Arc<T>,
}

///
/// Allows access to the modules `ExternCallback`.
/// Can be used to send `FunctionRequest` to the host application.
///
#[derive(Clone)]
pub struct CallExtern {
    callback: ExternCallback,
}

///
/// Allows a facade to be built using the extern_callback.
///
pub trait NewWithExternCallback {
    fn with_callback(extern_callback: ExternCallback) -> Self;
}

///
/// Build objects from container
///
pub trait FromContainer {
    fn from_container(container: &Container) -> Self;
}

///
/// Build objects from the call context
///
pub trait FromContext {
    fn from_context(context: &mut Context) -> Self;
}

///
/// Wraps a function. Allows to be called to a later point using a `Context`
///
pub struct Injectable<A, R> {
    function: Box<dyn Fn(A) -> R>,
}

pub trait CallableFunction: Send + Sync {
    fn call(&self, context: &mut Context) -> Buffer;
}

pub trait IntoInjectable<A, R> {
    fn into_injectable(self) -> Injectable<A, R>;
}

pub trait ArgType {
    type Type;
}

impl<A: FromContext, R: Serialize> CallableFunction for Injectable<A, R> {
    fn call(&self, context: &mut Context) -> Buffer {
        Buffer::from_struct(&self.run(context))
    }
}

unsafe impl<A, R> Send for Injectable<A, R> {}
unsafe impl<A, R> Sync for Injectable<A, R> {}

impl<T> ArgType for Data<T> {
    type Type = T;
}

impl<T> ArgType for Option<Data<T>> {
    type Type = Option<T>;
}

impl<T, E> ArgType for Result<Data<T>, E> {
    type Type = Result<T, E>;
}
