use crate::container::{Container, Context, FromContainer, FromContext};

impl FromContainer for () {
    fn from_container(_: &Container) -> Self {
        ()
    }
}

macro_rules! tuple_from_context {
    () => {};
    ($A: ident, $($arg: ident,)*) => {
        impl<$A: FromContext, $($arg: FromContext,)*> FromContext for ($A, $($arg,)*) {
            fn from_context(context: &mut Context) -> Self {
                ($A::from_context(context), $($arg::from_context(context),)*)
            }
        }

        tuple_from_context!($($arg,)*);
    };
}

tuple_from_context!(A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z,);

impl<T: FromContainer> FromContext for T {
    fn from_context(context: &mut Context) -> Self {
        T::from_container(context.container)
    }
}
