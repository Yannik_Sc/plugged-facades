use serde::{Deserialize, Serialize};

use serde::de::DeserializeOwned;

use crate::{Buffer, BufferError, ConvertDirection};
use serde::{Deserializer, Serializer};
use std::fmt::{Debug, Formatter};

impl Buffer {
    pub fn from_struct<S: Serialize>(data: &S) -> Self {
        Self::try_from_struct(data).unwrap_or(Self::from_bytes(Vec::new()))
    }

    pub fn try_from_struct<S: Serialize>(data: &S) -> Result<Self, BufferError> {
        Ok(Self::from_bytes(rmp_serde::to_vec(data).map_err(|_| {
            BufferError::ConversionError(ConvertDirection::from_type::<S>())
        })?))
    }

    pub fn from_bytes(data: Vec<u8>) -> Self {
        let boxed = Box::new(data);
        let len = boxed.len();
        let boxed = Box::into_raw(boxed);
        let data = unsafe { (*boxed).as_slice().as_ptr() };

        Self { data, boxed, len }
    }

    pub fn into_struct<D: DeserializeOwned>(self) -> Result<D, BufferError> {
        let bytes = unsafe { std::slice::from_raw_parts(self.data, self.len) };

        Ok(rmp_serde::from_slice(bytes)
            .map_err(|_| BufferError::ConversionError(ConvertDirection::to_type::<D>()))?)
    }

    pub fn to_struct<D: DeserializeOwned>(&self) -> Result<D, BufferError> {
        Ok(rmp_serde::from_slice(self.as_slice())
            .map_err(|_| BufferError::ConversionError(ConvertDirection::to_type::<D>()))?)
    }

    pub fn into_bytes(self) -> Vec<u8> {
        let bytes = unsafe { std::slice::from_raw_parts(self.data, self.len) };

        Vec::from(bytes)
    }

    pub fn as_slice<'a>(&self) -> &'a [u8] {
        unsafe { std::slice::from_raw_parts(self.data, self.len) }
    }
}

impl Clone for Buffer {
    fn clone(&self) -> Self {
        Buffer::from_bytes(Vec::from(self.as_slice()))
    }
}

impl Debug for Buffer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        <&[u8]>::fmt(&self.as_slice(), f)
    }
}

impl Serialize for Buffer {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        // TODO: Check how the .clone() impacts performance here
        Vec::<u8>::serialize(&self.clone().into_bytes(), serializer)
    }
}

impl<'de> Deserialize<'de> for Buffer {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let res = Vec::<u8>::deserialize(deserializer)?;

        Ok(Buffer::from_bytes(res))
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        unsafe {
            std::mem::drop(Box::from_raw(self.boxed));
        }
    }
}
