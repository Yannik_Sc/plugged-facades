use crate::CommandError;
use framework_library::prelude::CallExtern;
use framework_library::FunctionRequest;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::path::PathBuf;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::{Mutex, RwLock};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct CommandCall {
    pub name: String,
    #[serde(default)]
    pub args: Vec<Value>,
}

#[derive(Debug)]
pub struct CommandRegistry {
    fifo_path: PathBuf,
    handlers: RwLock<HashMap<String, String>>,
}

pub struct CommandStatus {
    receiver: Mutex<Option<Receiver<bool>>>,
    sender: Sender<bool>,
}

impl CommandRegistry {
    pub fn new<P: Into<PathBuf>>(fifo_path: P) -> Self {
        Self {
            fifo_path: fifo_path.into(),
            handlers: Default::default(),
        }
    }

    pub fn register_command(&self, name: String, callback: String) {
        println!(
            "Registering command \"{}\" for handler \"{}\"",
            name, callback
        );

        self.handlers.write().unwrap().insert(name, callback);
    }

    pub fn handle_backlog(&self, call_extern: &CallExtern) -> Result<(), CommandError> {
        let fifo = File::open(&self.fifo_path).map_err(|_| {
            CommandError::UnableToOpenFile(self.fifo_path.to_str().unwrap_or_default().to_string())
        })?;

        let mut reader = BufReader::new(fifo);

        loop {
            let mut buf = String::new();

            if reader
                .read_line(&mut buf)
                .map_err(|_| CommandError::UnableToReadLine)?
                == 0
            {
                break;
            }

            let call = serde_json::from_str::<CommandCall>(buf.as_str())
                .map_err(|err| CommandError::CommandParseError(format!("{}", err)))?;

            let handler = if let Some(handler) = self.handlers.read().unwrap().get(&call.name) {
                handler.clone()
            } else {
                continue;
            };

            let mut request = FunctionRequest::new(handler);

            for arg in call.args {
                request = request.add_arg(&arg);
            }

            call_extern.call_untyped(request);
        }

        Ok(())
    }
}

impl CommandStatus {
    pub fn new() -> Self {
        let (sender, recv) = channel();

        Self {
            receiver: Mutex::new(Some(recv)),
            sender,
        }
    }

    pub fn get_recv(&self) -> Receiver<bool> {
        let mut locked = self.receiver.lock().unwrap();

        let took = locked.take();
        *locked = None;

        took.expect("Receiver is already taken")
    }

    pub fn send_stop(&self) {
        self.sender.send(true).ok();
    }
}
