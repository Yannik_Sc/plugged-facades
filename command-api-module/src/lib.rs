pub mod commands;

use framework_library::prelude::*;
use serde::{Deserialize, Serialize};

use std::fmt::Formatter;

use crate::commands::{CommandRegistry, CommandStatus};
use framework_library::exports;
use std::time::Duration;

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ModuleConfig {
    pub fifo_path: String,
}

exports! {
    let container = crate::get_container();
    let extern_callback = crate::extern_callback;
    mod command_api_module {
        fn main;
        fn stop;
        fn register_command;
    };

    addon extern_callback;
    addon container(crate::setup_container);
    addon config(crate::ModuleConfig);
}

fn setup_container(container: Container) -> Container {
    container.register(CommandRegistry::new("commands.fifo"));
    container.register(CommandStatus::new());

    container
}

pub mod api {
    framework_library::api_fn!(crate::main());
    framework_library::api_fn!(crate::stop());
    framework_library::api_fn!(crate::register_command(String, String));
}

#[derive(Clone, Debug)]
pub enum CommandError {
    UnableToOpenFile(String),
    UnableToReadLine,
    CommandParseError(String),
}

impl std::error::Error for CommandError {}

impl std::fmt::Display for CommandError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::UnableToOpenFile(path) => {
                f.write_fmt(format_args!("Could not open fifo file '{}'", path))
            }
            Self::UnableToReadLine => f.write_str("Unable to read line from fifo"),
            Self::CommandParseError(_) => f.write_str("Unable to parse command"),
        }
    }
}

pub fn main(
    registry: Service<CommandRegistry>,
    status: Service<CommandStatus>,
    call_extern: CallExtern,
) {
    let registry = registry.into_inner();
    let status_receiver = status.get_recv();

    registry.register_command("stop".to_string(), api::stop().name());

    println!("Starting command_api_module");

    loop {
        if let Err(error) = registry.handle_backlog(&call_extern) {
            eprintln!("{}", error);

            break;
        }

        if let Ok(true) = status_receiver.recv_timeout(Duration::from_millis(100)) {
            break;
        }
    }

    println!("Stopping command_api_module");
}

pub fn stop(status: Service<CommandStatus>) {
    println!("Received stop command");
    status.send_stop();
}

pub fn register_command(
    callback_name: Data<String>,
    command_name: Data<String>,
    registry: Service<CommandRegistry>,
) {
    registry.register_command(command_name.into_inner(), callback_name.into_inner());
}
