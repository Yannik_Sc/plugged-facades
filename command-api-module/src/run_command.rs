use command_api_module::commands::CommandCall;
use std::fs::File;

fn main() {
    let mut cli_args = std::env::args().skip(1);
    let name = cli_args.next().expect("Missing command name");
    let mut args = Vec::new();

    for arg in cli_args {
        args.push(
            serde_yaml::from_str::<serde_json::Value>(arg.as_str())
                .expect("Could not parse CLI argument"),
        );
    }

    let command_call = CommandCall { name, args };

    let file = File::options()
        .write(true)
        .open("./commands.fifo")
        .expect("Unable to open fifo file");

    serde_json::to_writer(file, &command_call).expect("Could not write command");
}
