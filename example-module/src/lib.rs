use framework_library::exports;
use framework_library::prelude::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::time::Duration;

exports! {
    let container = crate::get_container();
    let extern_callback = crate::extern_callback;
    mod example_module {
        fn greet;
        fn greet_command;
        fn groot;
        fn example_handler;
        fn listener;
        fn get_config;
    };

    addon extern_callback;
    addon container(crate::setup_container);
}

pub fn setup_container(container: Container) -> Container {
    container.register(GreetingService);

    container
}

mod api {
    framework_library::api_fn!(crate::greet(String) -> String);
}

pub struct GreetingService;

impl GreetingService {
    pub fn greet(&self, name: &String) -> String {
        format!("Hello {}", name)
    }
}

pub fn some_method(arg: String) {
    println!("{}", arg);
}

pub fn greet_command(name: Result<Data<String>, DataError>, connection: CallExtern) {
    let greet = connection
        .call(api::greet(
            name.map(|val| val.into_inner())
                .unwrap_or(String::from("Undefined")),
        ))
        .unwrap();

    println!("{}", greet);
}

pub fn greet(name: Data<String>, service: Service<GreetingService>) -> String {
    service.greet(&name.into_inner())
}

pub fn groot() {
    println!("Called groot");
}

#[derive(Debug, Deserialize, Serialize)]
pub struct HandlerOptions {
    r#if: HashMap<String, String>,
}

pub fn example_handler(callable_name: Data<String>, options: Option<Data<HandlerOptions>>) {
    println!("Handling {} as example", *callable_name);

    if let Some(options) = options {
        println!("With options: {:#?}", *options);
    }
}

pub fn listener() {
    println!("Listening for stuff for 2 seconds");

    std::thread::sleep(Duration::from_millis(2000));
}

pub fn get_config(config: Data<HashMap<String, String>>) {
    println!("Got config: {:#?}", *config);
}
